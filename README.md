# remind-me
A Python 2.7 application which automatically reminds the users for competitions being organized on platforms like Hacker Rank and Code Chef. This application uses Requests and Beautiful Soup modules of python to traverse and fetch contents from those websites.
